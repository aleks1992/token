<?php

/**
 * Контроллер управления API Запросами
 */

namespace app\controllers;


use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class ApiController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionEvent()
    {
        $body = json_decode(Yii::$app->getRequest()->getRawBody(), true);
        if (is_array($body)
            && array_key_exists('cardId', $body)
            && array_key_exists('pinCode', $body)
            && array_key_exists('type', $body)
        ) {
            $card = (new \yii\db\Query())
                ->select(['id'])
                ->from('cards')
                ->where(["id" => $body['cardId']])
                ->where(['pinCode' => $body['pinCode']])
                ->one();

            if (!array_key_exists('id', $card))
                return $this->getJson(false, 'Error: is not card');

            if ($body['type'] == "in") {
                Yii::$app->db->createCommand()->insert("events", [
                    'CardId' => $card['id'],
                    'timeIn' => date("Y-m-d H:i:s"),
                ])->execute();
                return $this->getJson(true, 'Succes in');

            } else if ($body['type'] == "out") {
                $event = (new \yii\db\Query())
                    ->select(['id'])
                    ->from('events')
                    ->where(["cardId" => (int)$card['id']])
                    ->andWhere(['is', 'timeOut', new \yii\db\Expression('null')])
                    ->one();
                if (!array_key_exists('id', $event))
                    return $this->getJson(false, 'Error: is not In');
                Yii::$app->db->createCommand()->update('events', ['timeOut' => date("Y-m-d H:i:s")], "id = :id", ['id' => (int)$event['id']])->execute();
                return $this->getJson(true, 'Succes out');
            }

        }
        return $this->getJson();
    }

    /**
     * Получения json
     * @param bool $succes
     * @param string $message
     * @param null $data
     * @return false|string
     */
    private function getJson($succes = false, $message = "Error", $data = null)
    {
        $result = ['succes', 'message'];
        if (!is_null($data))
            array_push($result, 'data');

        return json_encode(compact($result));
    }
}