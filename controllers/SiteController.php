<?php

namespace app\controllers;

use app\models\RegisterForm;
use app\models\ResetPasswordForm;
use app\models\UpdatePasswordForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post','get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->render('index');
        }
        return $this->redirect('site/login');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        if (!$model->isGoogle)
            $model->password = '';
        return $this->render('login', [
            'model' => $model
        ]);
    }

    public function actionResetPassword()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new ResetPasswordForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->sendEmail()) {
                return $this->render('after-reset-password', [
                    'isReset' => true,
                ]);
            }
        }

        return $this->render('reset-password', [
            'model' => $model,
        ]);
    }

    public function actionRegister()
    {
        $model = new RegisterForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->register()) {
                return $this->render('after-register');
            }
        }

        return $this->render('register', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        if (!Yii::$app->user->isGuest) {
        Yii::$app->user->logout();
        }

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionUsers()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->render('users',$this->GetUserInfo());
        }
        return $this->redirect('login');
    }

    /**
     * Точки входа
     * @return string|Response
     */
    public function actionSites(){
        if (!Yii::$app->user->isGuest) {
            return $this->render('sites',$this->GetUserInfo());
        }
        return $this->redirect('login');
    }

    /**
     * @return string|Response
     */
    public function actionCards(){
        if (!Yii::$app->user->isGuest) {
            return $this->render('cards',$this->GetUserInfo());
        }
        return $this->redirect('login');
    }

    /**
     * @return string|Response
     */
    public function actionEvents(){
        if (!Yii::$app->user->isGuest) {
            return $this->render('event',$this->GetUserInfo());
        }
        return $this->redirect('login');
    }

    /**
     * Настройки пользователя
     * @return string|Response
     */
    public function actionSetting()
    {
        if (!Yii::$app->user->isGuest) {
            $users = (new \yii\db\Query())
                ->select(['is_ga'])
                ->from('users')
                ->where(["id" => (int)Yii::$app->user->getId()])
                ->one();

            $ga_secret = ($users['is_ga'] == 0)?false:true;
            $uri = Yii::$app->params['googleAccounts'];
            return $this->render('setting',['ga_secret'=>$ga_secret,'uri'=>$uri]);
        }
        return $this->redirect('login');
    }

    /**
     * Потверждения Email
     * @param $code
     * @param $email
     * @return Response
     */
    public function actionConfirmedEmail($code, $email)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $isConfirmedEmail = false;

        if (!empty($code) && !empty($email)) {
            $users = (new \yii\db\Query())
                ->select(['id'])
                ->from('users')
                ->where(['email' => $email, 'auth_key' => $code, 'confirmed_by_email' => false])
                ->one();

            if (!empty($users) && is_array($users) && array_key_exists('id', $users)) {
                Yii::$app->db->createCommand()->update('users', ['confirmed_by_email' => true], "id = :id", ['id' => (int)$users['id']])->execute();
                $isConfirmedEmail = true;
            }
        }

        return $this->render("confirmed-email", ["isConfirmedEmail" => $isConfirmedEmail]);

    }

    /**
     * Получения авторизованного пользовавтеля
     * @return array|bool|null
     */
    public function GetUserInfo($id = null)
    {
        $info = ["addEl" => false, "isAdmin"=>"false"];
        if (!Yii::$app->user->isGuest || !empty($id)) {
            $user = (new \yii\db\Query())
                ->select(['*'])
                ->from('users')
                ->where(["id" => (!empty($id)) ? $id : Yii::$app->user->getId()])
                ->one();
        }
        $info["addEl"] = (intval($user['parentId']) == 0);
        $info["isAdmin"] = (intval($user['isAdmin']) !== 0);
        return $info;
    }

    public function actionUpdatePassword(){



        if (Yii::$app->user->isGuest) {

            $model = new UpdatePasswordForm();
            if ($model->load(Yii::$app->request->post()) && $model->update()) {
                return $this->render("after-update-password", ['isReset'=>true]);
            }
            $body = Yii::$app->request->get();
            if (!empty($body['email']) && !empty($body['code'])) {
                $model->password = null;
                $model->email = $body['email'];
                $model->code = $body['code'];
                $user = (new \yii\db\Query())
                    ->select(['*'])
                    ->from('users')
                    ->where(["email" => $model->email, "password_reset_token" => $model->code])
                    ->one();
                if (is_array($user) && array_key_exists('id', $user)) {
                    return $this->render("update-password", ['model'=>$model]);
                }
            }
        }
        return $this->goHome();
    }

}
