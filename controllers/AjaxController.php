<?php


namespace app\controllers;


use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class AjaxController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Получения списка Sites
     * @return false|string
     */
    public function actionGetListSites()
    {
        if (!Yii::$app->user->isGuest) {
            $user = $this->GetUser();
            $where = ["userId" => $user["id"]];
            if (boolval($user['isAdmin'])) {
                $where = null;
            } else if (intval($user["parentId"]) !== 0) {
                $where = ["userId" => $user["parentId"]];
            }

            $data = empty($where) ? (new \yii\db\Query())
                ->select(['*'])
                ->from('sites')
                ->all() : (new \yii\db\Query())
                ->select(['*'])
                ->from('sites')
                ->where($where)
                ->all();

            return $this->getJson(true, '', $data);
        }

        return $this->getJson();
    }

    /**
     * Добавления нового Sites
     * @return false|string
     */
    public function actionAddSites()
    {
        $body = json_decode(Yii::$app->getRequest()->getRawBody(), true);

        if (!Yii::$app->user->isGuest && is_array($body) && array_key_exists('name', $body)) {
            $sites = (new \yii\db\Query())
                ->select(['id'])
                ->from('sites')
                ->where(["name" => $body['name']])
                ->all();

            if (count($sites) !== 0)
                return $this->getJson(false, 'Name is busy');
            Yii::$app->db->createCommand()->insert("sites", [
                'name' => $body['name'],
                'description' => $body['description'],
                'userId' => Yii::$app->getUser()->getId(),
                'apiToken' => Yii::$app->security->generateRandomString(),
                'isApiToken' => $body['apiAuthorization']
            ])->execute();
            return $this->getJson(true, 'Site save');


        }


        return $this->getJson();
    }

    public function actionGetListCards()
    {
        if (!Yii::$app->user->isGuest) {
            $user = $this->GetUser();
            $where = ["s.userId" => $user["id"]];
            if (boolval($user['isAdmin'])) {
                $where = null;
            } else if (intval($user["parentId"]) !== 0) {
                $where = ["s.userId" => $user["parentId"]];
            }
            $data = (empty($where)) ? (new \yii\db\Query())
                ->select(['*'])
                ->from('cards')
                ->leftJoin(['s' => 'sites'], 's.id = siteId')
                ->all() : (new \yii\db\Query())
                ->select(['*'])
                ->from('cards')
                ->leftJoin(['s' => 'sites'], 's.id = siteId')
                ->where($where)
                ->all();

            return $this->getJson(true, '', $data);
        }

        return $this->getJson();
    }

    public function actionAddCard()
    {
        $body = json_decode(Yii::$app->getRequest()->getRawBody(), true);

        if (!Yii::$app->user->isGuest && is_array($body)
            && array_key_exists('userId', $body)
            && array_key_exists('siteId', $body)
            && array_key_exists('pinCode', $body)
        ) {
            Yii::$app->db->createCommand()->insert("cards", [
                'description' => $body['description'],
                'userId' => $body['userId'],
                'siteId' => $body['siteId'],
                'pinCode' => $body['pinCode']
            ])->execute();
            return $this->getJson(true, 'Card save');


        }


        return $this->getJson();
    }

    public function actionGetListUsers()
    {
        if (!Yii::$app->user->isGuest) {
            $data = (new \yii\db\Query())
                ->select(['id', 'username'])
                ->from('users')
                ->all();

            return $this->getJson(true, '', $data);
        }

        return $this->getJson();
    }

    public function actionUsersGetList()
    {
        if (!Yii::$app->user->isGuest) {
            $user = $this->GetUser();
            $where = ["parentId" => $user["id"]];
            if (boolval($user['isAdmin'])) {
                $where = null;
            } else if (intval($user["parentId"]) !== 0) {
                $where = ["parentId" => $user["parentId"]];
            }
            $data = (empty($where)) ? (new \yii\db\Query())
                ->select(['*'])
                ->from('users')
                ->all() : (new \yii\db\Query())
                ->select(['*'])
                ->from('users')
                ->where($where)
                ->all();

            return $this->getJson(true, '', $data);
        }

        return $this->getJson();
    }

    public function actionUserAdd()
    {
        $body = json_decode(Yii::$app->getRequest()->getRawBody(), true);
        if (!Yii::$app->user->isGuest &&
            is_array($body) &&
            array_key_exists('name', $body) &&
            array_key_exists('password', $body) &&
            array_key_exists('type', $body) &&
            array_key_exists('email', $body)
        ) {
            $user = (new \yii\db\Query())
                ->select(['*'])
                ->from('users')
                ->where(["username" => $body['name']])
                ->all();
            if (count($user) !== 0)
                return $this->getJson(false, 'Error: user name');
            $user = (new \yii\db\Query())
                ->select(['*'])
                ->from('users')
                ->where(["email" => $body['email']])
                ->all();
            if (count($user) !== 0)
                return $this->getJson(false, 'Error: email');
            $user = new User();
            $U = $this->GetUser();
            if (intval(!$U['isAdmin']) && (intval($body['type'] !== 3)))
                return $this->getJson(false, 'Error: Invalid rights');

            $user->username = $body['name'];
            $user->email = $body['email'];
            $user->setPassword($body['password']);
            $user->generateAuthKey();
            $isAdmin = intval($body['type']) === 0;
            $parrent = (intval($body['type']) === 3) ? intval($U['id']) : 0;
            if ($user->save()) {
                Yii::$app->db->createCommand()->update('users',
                    [
                        'isAdmin' => $isAdmin,
                        'parentId' => $parrent
                    ],
                    "id = :id", ['id' => (int)$user->getId()]
                )->execute();;
                $uri = Yii::$app->urlManager->getHostInfo() . Yii::$app->urlManager->createUrl(['site/confirmed-email', "code" => $user->getAuthKey(), "email" => $user->email]);
                Yii::$app->mailer->compose()
                    ->setTo($user->email)
                    ->setFrom([Yii::$app->params['supportEmail'] => 'Account Confirmations'])
                    ->setSubject("Account Confirmations")
                    ->setHtmlBody("Hello <br> To verify your account, go to <a href='" . $uri . "'>link</a>")
                    ->send();
                return $this->getJson(true, 'User add');
            }

        }
        return $this->getJson();
    }

    public function actionGetListEvent()
    {
        if (!Yii::$app->user->isGuest) {
            $data = (new \yii\db\Query())
                ->select(['*'])
                ->from('events')
                ->all();

            return $this->getJson(true, '', $data);
        }

        return $this->getJson();
    }

    public function actionGetListEvents()
    {
        if (!Yii::$app->user->isGuest) {
            $data = $this->getListEvents();
            return $this->getJson(true, '', $data);
        }

        return $this->getJson();
    }

    public function actionSaveEvents()
    {
        $list = $this->getListEvents();
        $delimiter =";";
        $filename = "exportEvent.csv";
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'";');

        $f = fopen('php://output', 'w');
        $i =1;

        $fields = [
            "#",
            'Card Id',
            'time In',
            'time Out'
        ];

        fputcsv($f, $fields,$delimiter);

        foreach ($list as $line) {
            $fields = [
                $i,
                $line['id'],
                $line['timeIn'],
                $line['timeOut']
            ];
            fputcsv($f, $fields, $delimiter);
            $i++;
        }
    }

    private function getListEvents()
    {
        if (!Yii::$app->user->isGuest) {
            $user = $this->GetUser();
            $where = ["s.userId" => Yii::$app->getUser()->getId()];
            if (boolval($user['isAdmin'])) {
                $where = null;
            } else if (intval($user["parentId"]) !== 0) {
                $where = ["s.userId" => $user["parentId"]];
            }
            return empty($where) ?
                (new \yii\db\Query())
                    ->select(['*'])
                    ->from('events')
                    ->leftJoin(['c' => 'cards'], 'c.id = cardId')
                    ->leftJoin(['s' => 'sites'], 's.id = c.siteId')
                    ->all()
                : (new \yii\db\Query())
                    ->select(['*'])
                    ->from('events')
                    ->leftJoin(['c' => 'cards'], 'c.id = cardId')
                    ->leftJoin(['s' => 'sites'], 's.id = c.siteId')
                    ->where($where)
                    ->all();
        }

    }

    public function GetUser($id = null)
    {
        $user = null;
        if (!Yii::$app->user->isGuest || !empty($id)) {
            $user = (new \yii\db\Query())
                ->select(['*'])
                ->from('users')
                ->where(["id" => (!empty($id)) ? $id : Yii::$app->user->getId()])
                ->one();
        }
        return $user;
    }

    /**
     * Получения json
     * @param bool $succes
     * @param string $message
     * @param null $data
     * @return false|string
     */
    private function getJson($succes = false, $message = "authorisation Error", $data = null)
    {
        $result = ['succes', 'message'];
        if (!is_null($data))
            array_push($result, 'data');

        return json_encode(compact($result));
    }
}