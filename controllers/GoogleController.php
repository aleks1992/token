<?php


namespace app\controllers;


use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class GoogleController extends Controller
{
    static $PASS_CODE_LENGTH = 6;
    static $PIN_MODULO;
    static $SECRET_LENGTH = 10;

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function installGoogle()
    {
        self::$PIN_MODULO = pow(10, self::$PASS_CODE_LENGTH);
    }

    /**
     * Гкнерирует с сохряняет код доступа пользователя
     * @return false|string
     * @throws \yii\db\Exception
     */
    public function actionGetCode()
    {
        if (!Yii::$app->user->isGuest) {
            $this->installGoogle();
            $code = $this->generateSecret();
            Yii::$app->db->createCommand()->update('users', ['ga_secret' => $code], "id = :id", ['id' => (int)Yii::$app->user->getId()])->execute();

            return $this->getJson(true, 'save code');
        }

        return $this->getJson();
    }

    /**
     * Проверят код достпу включает Two Factor Authentication
     * @param $code
     * @return false|string
     * @throws \yii\db\Exception
     */
    public function actionSaveGoogle()
    {
        $body = json_decode(Yii::$app->getRequest()->getRawBody(), true);
        if (!Yii::$app->user->isGuest &&
            is_array($body) &&
            array_key_exists('code', $body)
        ) {
            $code = $body['code'];
            $this->installGoogle();
            $user = $this->GetUser();
            $codeGoogle = $this->getCode($user['ga_secret']);
            if ($code == $codeGoogle) {
                Yii::$app->db->createCommand()->update('users', ['is_ga' => true], "id = :id", ['id' => (int)$user['id']])->execute();
                return $this->getJson(true, 'Two Factor Authentication add');
            } else {
                return $this->getJson(true, 'Error: code');
            }
        }

        return $this->getJson();
    }

    /**
     * Удаляет Two Factor Authentication
     * @param $code
     * @return false|string
     * @throws \yii\db\Exception
     */
    public function actionRemoveGoogle()
    {

        if (!Yii::$app->user->isGuest) {
            $user = $this->GetUser();
            Yii::$app->db->createCommand()->update('users', ['is_ga' => false], "id = :id", ['id' => (int)$user['id']])->execute();
            return $this->getJson(true, 'Two Factor Authentication remove');

        }

        return $this->getJson();
    }


    public function isValidCode($id, $code)
    {
        $user = $this->GetUser($id);
        $isValid = true;
        if ($user !== null) {
            $this->installGoogle();
            $codeGoogle = $this->getCode($user['ga_secret']);
            if (boolval($user['is_ga'])) {
                if ($code == $codeGoogle) {
                    $isValid = true;
                } else {
                    $isValid = false;
                }
            }
        }
        return $isValid;
    }

    /**
     * Получения авторизованного пользовавтеля
     * @return array|bool|null
     */
    public function GetUser($id = null)
    {
        $user = null;
        if (!Yii::$app->user->isGuest || !empty($id)) {
            $user = (new \yii\db\Query())
                ->select(['*'])
                ->from('users')
                ->where(["id" => (!empty($id)) ? $id : Yii::$app->user->getId()])
                ->one();
        }
        return $user;
    }

    /**
     * Сборка Uri на шорт код
     * @return false|string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGetUri()
    {
        if (!Yii::$app->user->isGuest) {
            $this->installGoogle();
            $user = $this->GetUser();
            $uri = $this->getUrl($user['username'], Yii::$app->urlManager->getHostInfo(), $user['ga_secret']);
            return $this->getJson(true, '', ['uri' => base64_encode($uri)]);
        }
        return $this->getJson();
    }

    public function actionGetImg($uri)
    {
        if (!empty($uri)) {
            $fp = fopen(base64_decode($uri), 'rb');
            if ($fp !== false) {
                header("Content-Type: image/png");
                fpassthru($fp);
            }
        } else {
            status_header(404);
        }
    }

    /**
     * Получения json
     * @param bool $succes
     * @param string $message
     * @param null $data
     * @return false|string
     */
    private function getJson($succes = false, $message = "google Error", $data = null)
    {
        $result = ['succes', 'message'];
        if (!is_null($data))
            array_push($result, 'data');

        return json_encode(compact($result));
    }


    public function checkCode($secret, $code)
    {
        $time = floor(time() / 30);
        for ($i = -1; $i <= 1; $i++) {

            if ($this->getCode($secret, $time + $i) == $code) {
                return true;
            }
        }

        return false;

    }

    public function getCode($secret, $time = null)
    {

        if (!$time) {
            $time = floor(time() / 30);
        }
        list($base32) = Yii::$app->createController('fixed');

        $base32->install(5, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567', TRUE, TRUE);
        $secret = $base32->decode($secret);

        $time = pack("N", $time);
        $time = str_pad($time, 8, chr(0), STR_PAD_LEFT);

        $hash = hash_hmac('sha1', $time, $secret, true);
        $offset = ord(substr($hash, -1));
        $offset = $offset & 0xF;

        $truncatedHash = self::hashToInt($hash, $offset) & 0x7FFFFFFF;
        $pinValue = str_pad($truncatedHash % self::$PIN_MODULO, 6, "0", STR_PAD_LEFT);;
        return $pinValue;
    }

    protected function hashToInt($bytes, $start)
    {
        $input = substr($bytes, $start, strlen($bytes) - $start);
        $val2 = unpack("N", substr($input, 0, 4));
        return $val2[1];
    }

    public function getUrl($user, $hostname, $secret)
    {
        $url = sprintf("otpauth://totp/%s@%s?secret=%s", $user, $hostname, $secret);
        $encoder = "https://www.google.com/chart?chs=200x200&chld=M|0&cht=qr&chl=";
        $encoderURL = sprintf("%sotpauth://totp/%s@%s?secret=%s", $encoder, $user, $hostname, $secret);

        return $encoderURL;

    }

    public function generateSecret()
    {
        $secret = "";
        for ($i = 1; $i <= self::$SECRET_LENGTH; $i++) {
            $c = rand(0, 255);
            $secret .= pack("c", $c);
        }
        list($base32) = Yii::$app->createController('fixed');
        $base32->install(5, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567', TRUE, TRUE);
        return $base32->encode($secret);


    }


}