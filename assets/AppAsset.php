<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $depends = [
        'yii\web\YiiAsset',
        /* 'yii\bootstrap\BootstrapAsset',*/
    ];

    public $css = [
        'css/site.css',
        'assets/argon/vendor/nucleo/css/nucleo.css',
        'assets/argon/vendor/font-awesome/css/font-awesome.min.css',
        'assets/argon/css/argon.css?v=1.1.0'
    ];
    public $js = [
        'assets/argon/vendor/popper/popper.min.js',
        'assets/argon/vendor/bootstrap/bootstrap.min.js',
        'assets/argon/vendor/headroom/headroom.min.js',
        'assets/argon/vendor/onscreen/onscreen.min.js',
        'assets/argon/vendor/nouislider/js/nouislider.min.js',
        'assets/argon/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
        'assets/argon/js/argon.js?v=1.1.0',
        'js/vue.js',
        'js/mixin.js',
        'js/site.js',
        'js/card.js',
        'js/event.js',
        'js/setting.js',
        'js/user.js',
        'js/componets.js'
    ];

    /*
       <script src="./assets/vendor/jquery/jquery.min.js"></script>
  <script src="./assets/vendor/popper/popper.min.js"></script>
  <script src="./assets/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="./assets/vendor/headroom/headroom.min.js"></script>
  <!-- Optional JS -->
  <script src="./assets/vendor/onscreen/onscreen.min.js"></script>
  <script src="./assets/vendor/nouislider/js/nouislider.min.js"></script>
  <script src="./assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
  <!-- Argon JS -->
  <script src="./assets/js/argon.js?v=1.1.0"></script>
     * */
}
