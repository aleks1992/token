//Найстройка card
Vue.component('cards-block',{
    props: {
        addEl: {
            default: 1
        },
        isAdmin: {
            default: 0
        }
    },
    data: function(){
        return{
            list: null,
            loading: true,
            mgsErrorList:"Card list is emptyt out!",
            msgInfo:null,
            loadingAdd: false,
            addModel:{
                userId:null,
                siteId:null,
                description: null,
                pinCode: null
            }
        }
    },
    mixins: [httpClientMixin],
    methods:{
        updateList: function () {
            this.loading = true;
            this.list = null;
            var uri = "/web/ajax/get-list-cards";
            this.httpClient(uri,"GET",null,(result)=>{
                if ("data" in result && result.data.length > 0){
                    this.list = result.data;
                }
              //  console.log(this.list);
                this.loading = false
            });



        },
        addItem:function () {
            var uri = "/web/ajax/add-card";
            this.msgInfo = null;
            this.loadingAdd = true;
            this.httpClient(uri,"POST",this.addModel,(result)=>{
                this.loadingAdd = false;
                this.msgInfo = result.message;
                this.updateList();
                if (result.succes) {
                    this.addModel.pinCode = null;
                    this.addModel.description = null;
                    var self = this;
                    setTimeout(() => self.msgInfo=null, 10000);
                }
            });
        }
    },
    template:`
   <div>
        <div class="row"> 
        <template v-if="JSON.parse(addEl)"> 
            <div class="col-lg-4 col-md-4 col-sm-12">
                <button type="button" class="btn btn-block btn-success mb-3" data-toggle="modal" data-target="#modal-add-site">
                    <i class="fas fa-folder-plus"></i>&nbsp;Add Card
                </button>   
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12">
                <template v-if="msgInfo !== null">
                   <div class="alert alert-info alert-dismissible min-allert" role="alert">
                    <span class="alert-inner--icon"><i class="ni ni-bell-55"></i></span>
                    <span class="alert-inner--text">{{ msgInfo }}</span>
                  </div>
                </template>
                <template v-if="msgInfo !== null && loadingAdd === true">
                    <p class="text-info mb-0"><span class="bm-loading-spinner lg"></span> Loding...</p>
                </template>
            </div>
         </template>
        </div>
        <template v-if="list === null && loading === false">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="alert-inner--icon"><i class="ni ni-bell-55"></i></span>
                <span class="alert-inner--text">{{ mgsErrorList }}</span>
            </div>
        </template>
        <template v-else-if="loading === true && list === null">
            <div class="flex-center">
                <span class="bm-loading-spinner lg"></span>
            </div>
        </template>
        <template v-else>
            <div class="row list-header"> 
                <div class="col-lg-1 col-md-1 col-sm-12">
                    №
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12">
                    Id Site
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    Pin Code
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12">
                    Description
                </div>
            </div>
            <template v-for="(item,index) in list">
                 <div class="row list-row"> 
                    <div class="col-lg-1 col-md-1 col-sm-12">
                        {{ index+1 }}
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12">
                        {{ item.siteId }}
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        {{ item.pinCode }}
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12">
                        {{ item.description }}
                    </div>
                </div>
            </template>
        </template>
        <card-add v-on:addModel="addItem" :add-model="addModel" ></card-add>    
   </div>       
    `,
    created: function () {
        this.updateList();
        this.$on('addModel');

    }
});

/**
 * Добавления нового card
 */
Vue.component('card-add',{
    props:["addModel"],
    data:function(){
        return{
            msgError: null,
            listSite: null,
            listUser: null
        }
    },
    mixins: [httpClientMixin],
    methods:{
        updateModal:function(){
            this.msgError = null;
            console.log(this.addModel);
            if (this.addModel.pinCode === null){
                this.msgError = "field pinCode must not be empty";
                return false;
            }
            if (this.addModel.userId === null){
                this.msgError = "field User must not be empty";
                return false;
            }
            if (this.addModel.siteId === null){
                this.msgError = "field Site must not be empty";
                return false;
            }


            this.$emit('addModel');
            $('#modal-add-site').modal('hide');
        },
        getListSites:function () {
            this.listSite = null;
            var uri = "/web/ajax/get-list-sites";
            this.httpClient(uri,"GET",null,(result)=>{
                if ("data" in result && result.data.length > 0){
                    this.listSite = result.data;
                }
            });
        },
        getListUsers :function () {
            this.listUser = null;
            var uri = "/web/ajax/get-list-users";
            this.httpClient(uri,"GET",null,(result)=>{
                if ("data" in result && result.data.length > 0){
                    this.listUser = result.data;
                }
            });
        }
    },
    template: `
    <div class="modal fade" id="modal-add-site" tabindex="-1" role="dialog" aria-labelledby="modal-default" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h6 class="modal-title" id="modal-title-default"><i class="fas fa-folder-plus"></i></i>&nbsp;Add Card</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">
                          <div class="form-group">
                            <input type="text" placeholder="Pin Code" v-model="addModel.pinCode" class="form-control form-control-alternative">
                          </div>
                        </div>
                         <div class="col-lg-12 col-sm-12">
                          <div class="form-group">
                            <textarea rows="5" cols="5" placeholder="Description" v-model="addModel.description" class="form-control form-control-alternative"></textarea>
                          </div>
                        </div>
                        <div class="col-lg-12 col-sm-12">
                           <template v-if="listSite !== null">
                                <div class="form-group">
                                    <select class="form-control form-control-alternative" v-model="addModel.siteId"> <!--Supplement an id here instead of using 'name'-->
                                        <template v-for="(item,index) in listSite">
                                            <option :value="item.id">{{ item.name }}</option> 
                                        </template>
                                    </select>
                                    </div>
                            </template>
                            <template v-else>
                                <span class="text-warning mb-0">Site list is null</span>
                            </template>
                        </div>
                        <div class="col-lg-12 col-sm-12">
                           <template v-if="listUser !== null">
                                <div class="form-group">
                                    <select class="form-control form-control-alternative" v-model="addModel.userId"> <!--Supplement an id here instead of using 'name'-->
                                        <template v-for="(item,index) in listUser">
                                            <option :value="item.id">{{ item.username }}</option> 
                                        </template>
                                    </select>
                                    </div>
                            </template>
                            <template v-else>
                                <span class="text-warning mb-0">User list is null</span>
                            </template>
                        </div>
                       <!-- <div class="col-lg-12 col-sm-12">
                            <div class="custom-control custom-checkbox mb-3">
                              <input class="custom-control-input" id="customCheck1" type="checkbox" v-model="addModel.apiAuthorization">
                              <label class="custom-control-label" for="customCheck1">
                                <span>Api authorization</span>
                              </label>
                            </div>
                        </div>-->
                        <div class="col-lg-12 col-sm-12">
                            <div class="form-group">
                                <template v-if="msgError !== null">
                                    <div class="alert alert-warning alert-dismissible" role="alert">
                                        <span class="alert-inner--icon"><i class="ni ni-bell-55"></i></span>
                                         <span class="alert-inner--text">{{ msgError }}</span>
                                      </div>
                                   
                                </template>                
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary" v-on:click="updateModal">Add</button>
                  <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
    `,
    created:function () {
        this.getListSites();
        this.getListUsers();

    }
});
