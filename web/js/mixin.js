var httpClientMixin = {
    methods: {
        httpClient: function (url, method, data, callback) {
            $.ajax({
                url: url,
                method: method,
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify(data),
                success: result => {
                    if (callback !== null) {
                        callback(result);
                    }
                }
            })
        },
    }
};