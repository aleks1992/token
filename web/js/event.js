//Найстройка EVENTS
Vue.component('event-block',{
    props: {
        addEl: {
            default: false
        },
        isAdmin: {
            default: false
        }
    },
    data: function(){
        return{
            list: null,
            loading: true,
            mgsErrorList:"Events list is emptyt out!",
            msgInfo:null,
            loadingAdd: false,
            addModel:{
                name:null,
                description: null,
                apiAuthorization: false
            }
        }
    },
    mixins: [httpClientMixin],
    methods:{
        updateList: function () {
            this.loading = true;
            this.list = null;
            var uri = "/web/ajax/get-list-events";
            this.httpClient(uri,"GET",null,(result)=>{
                if ("data" in result && result.data.length > 0){
                    this.list = result.data;
                }
                this.loading = false
            });



        }
    },
    template:`
   <div>
        <div class="row"> 
            <div class="col-lg-4 col-md-4 col-sm-12">
                <a download="" href="/web/ajax/save-events" class="btn btn-block btn-success mb-3" >
                    <i class="fas fa-folder-plus"></i>&nbsp;Save
                </a>   
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12">
                <template v-if="msgInfo !== null">
                   <div class="alert alert-info alert-dismissible min-allert" role="alert">
                    <span class="alert-inner--icon"><i class="ni ni-bell-55"></i></span>
                    <span class="alert-inner--text">{{ msgInfo }}</span>
                  </div>
                </template>
                <template v-if="msgInfo !== null && loadingAdd === true">
                    <p class="text-info mb-0"><span class="bm-loading-spinner lg"></span> Loding...</p>
                </template>
            </div>
        </div>
        <template v-if="list === null && loading === false">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="alert-inner--icon"><i class="ni ni-bell-55"></i></span>
                <span class="alert-inner--text">{{ mgsErrorList }}</span>
            </div>
        </template>
        <template v-else-if="loading === true && list === null">
            <div class="flex-center">
                <span class="bm-loading-spinner lg"></span>
            </div>
        </template>
        <template v-else>
            <div class="row list-header"> 
                <div class="col-lg-1 col-md-1 col-sm-12">
                    №
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">
                    Card ID
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    Time IN
                </div>  
                <div class="col-lg-4 col-md-4 col-sm-12">
                    Time ON
                </div>
            </div>
            <template v-for="(item,index) in list">
                 <div class="row list-row"> 
                    <div class="col-lg-1 col-md-1 col-sm-12">
                        {{ index+1 }}
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        {{ item.cardId }}
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        {{ item.timeIn }}
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        {{ item.timeOut }}
                    </div>
                </div>
            </template>
        </template>
      
   </div>       
    `,
    created: function () {
        this.updateList();

    }
});

