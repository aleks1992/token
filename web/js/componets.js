





var vueEl = [
    {el: "user-block", loading: false},
    {el: "sites-block", loading: false},
    {el: "cards-block", loading: false},
    {el: "event-block",loading:false},
    {el: "setting-block",loading:false}

];

function vueLoading() {
    if (typeof Vue !== 'undefined') {
        vueEl.forEach((item, key) => {
            if (document.getElementById(item.el) && !item.loading) {
                new Vue({el: '#' + item.el});
                item.loading = true;
            }
        });
    }
}

document.addEventListener('DOMContentLoaded', function () {
    vueLoading();
});

