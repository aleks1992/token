Vue.component('setting-block', {
    props: ['gaSecret', 'uri'],
    data: function () {
        return {
            openModal: false,
            msgInfo: null,
            ga: false,
            dataModel: {
                code: null
            }

        }
    },
    methods: {
        addItem: function () {
            var uri = "/web/google/save-google";
            this.msgInfo = null;
            this.loadingAdd = true;
            this.httpClient(uri, "POST", this.dataModel, (result) => {
                this.loadingAdd = false;
                this.msgInfo = result.message;
                this.ga = this.succes;
                var self = this;
                setTimeout(() => self.msgInfo = null, 10000);
            })
        },
        removeGoogle: function () {
            var uri = "/web/google/remove-google";
            this.msgInfo = null;
            this.loadingAdd = true;
            this.httpClient(uri, "POST", null, (result) => {
                this.loadingAdd = false;
                this.msgInfo = result.message;
                var self = this;
                setTimeout(() => self.msgInfo = null, 10000);
                this.ga = !this.succes;
            })

        }
    },
    mixins: [httpClientMixin],
    template: `
    <div>
            <div class="row"> 
            <div class="col-lg-4 col-md-4 col-sm-12">
             <template v-if="!ga">
                <button type="button" class="btn btn-block btn-success mb-3" data-toggle="modal" data-target="#modal-add-google">
                    Add Two Factor Authentication
                </button>  
             </template>
             <template v-else>
                <button type="button" class="btn btn-block btn-warning mb-3" v-on:click="removeGoogle">
                    Remove Two Factor Authentication
                </button>  
             </template>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12">
                <template v-if="msgInfo !== null">
                   <div class="alert alert-info alert-dismissible min-allert" role="alert">
                    <span class="alert-inner--icon"><i class="ni ni-bell-55"></i></span>
                    <span class="alert-inner--text">{{ msgInfo }}</span>
                  </div>
                </template>
                <template v-if="msgInfo !== null && loadingAdd === true">
                    <p class="text-info mb-0"><span class="bm-loading-spinner lg"></span> Loding...</p>
                </template>
            </div>
        </div>
        <google-add :uri="uri" v-on:addModel="addItem" :data-model="dataModel"></google-add>
    </div>
    `,
    created: function () {
        this.ga = this.gaSecret;
        this.$on('addModel');

    }
});
Vue.component('google-add', {
    props: ['uri', 'dataModel'],
    data: function () {
        return {
            loding: false,
            getCode: false,
            backgroundUrl: null


        }
    },
    methods: {
        getCodeUser: function () {
            this.loading = true;
            var uri = "/web/google/get-code";
            this.httpClient(uri, "GET", null, (result) => {
                this.getUri();
            })
        },
        getUri: function () {
            var uri = "/web/google/get-uri";
            this.httpClient(uri, "GET", null, (result) => {
                this.loading = false;
                this.getCode = true;
                this.backgroundUrl = "/web/google/get-img?uri=" + result.data.uri;

            })
        },
        saveModal: function () {
            if (this.dataModel.code !== null) {
                this.$emit('addModel');
                $('#modal-add-google').modal('hide');
            }
        }
    },
    mixins: [httpClientMixin],
    template: `
    <div class="modal fade" id="modal-add-google" tabindex="-1" role="dialog" aria-labelledby="modal-default"  aria-hidden="true">
            <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h6 class="modal-title" id="modal-title-default">Two Factor Authentication</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <template v-if="!getCode && !loding">
                            <div class="col-lg-12 col-sm-12">
                                <button class="btn btn-block btn-success mb-3" v-on:click="getCodeUser">Two Factor Authentication</button>
                            </div>
                        </template>
                       <template v-else-if="loding && !getCode">
                            <div class="col-lg-12 col-sm-12">
                                <div class="flex-center">
                                        <span class="bm-loading-spinner lg"></span>
                                </div>
                            </div>
                        </template>
                        <template v-else-if="!loding && getCode">
                            <div class="col-lg-12 col-sm-12">
                                <span>Please install <a :href="uri" target="_blank">Google Authenticator</a>
                                in your phone/tablet, open it and then scan the bar code above to add this application to it. After you have added the application, enter the code you see in Authenticator into the box below and click "Save"
                                </span>
                            </div>
                            <div class="col-lg-12 col-sm-12">
                                <div class="flex-center">
                                     <img class="img-qr" :src="backgroundUrl">
                                </div>
                            </div>
                             <div class="col-lg-12 col-sm-12">
                                 <div class="form-group">
                                   <input type="text" placeholder="Code" v-model="dataModel.code" class="form-control form-control-alternative">
                                 </div>
                             </div>
                             <div class="col-lg-12 col-sm-12">
                                <button class="btn btn-block btn-success mb-3" v-on:click="saveModal">save</button>
                            </div>
                        </template>
                        
                    </div>
                </div>

              </div>
            </div>
          </div>
    `
});
