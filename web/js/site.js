//Найстройка SITES
Vue.component('sites-block',{
    props: {
        addEl: {
            default: 1
        },
        isAdmin: {
            default: 0
        }
    },
    data: function(){
        return{
            list: null,
            loading: true,
            mgsErrorList:"Sites list is emptyt out!",
            msgInfo:null,
            loadingAdd: false,
            addModel:{
                name:null,
                description: null,
                apiAuthorization: false
            }
        }
    },
    mixins: [httpClientMixin],
    methods:{
        updateList: function () {
            this.loading = true;
            this.list = null;
            var uri = "/web/ajax/get-list-sites";
            this.httpClient(uri,"GET",null,(result)=>{
                if ("data" in result && result.data.length > 0){
                    this.list = result.data;
                }
              //  console.log(this.list);
                this.loading = false
            });



        },
        addItem:function () {
            var uri = "/web/ajax/add-sites";
            this.msgInfo = null;
            this.loadingAdd = true;
            this.httpClient(uri,"POST",this.addModel,(result)=>{
                this.loadingAdd = false;
                this.msgInfo = result.message;
                this.updateList();
                if (result.succes) {
                    this.addModel.name = null;
                    this.addModel.description = null;
                    this.addModel.apiAuthorization = false;
                    var self = this;
                    setTimeout(() => self.msgInfo=null, 10000);
                }
            });
        }
    },
    template:`
   <div>
        <div class="row"> 
            <template v-if="JSON.parse(addEl)">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <button type="button" class="btn btn-block btn-success mb-3" data-toggle="modal" data-target="#modal-add-site">
                        <i class="fas fa-folder-plus"></i>&nbsp;Add site
                    </button>   
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12">
                    <template v-if="msgInfo !== null">
                       <div class="alert alert-info alert-dismissible min-allert" role="alert">
                        <span class="alert-inner--icon"><i class="ni ni-bell-55"></i></span>
                        <span class="alert-inner--text">{{ msgInfo }}</span>
                      </div>
                    </template>
                    <template v-if="msgInfo !== null && loadingAdd === true">
                        <p class="text-info mb-0"><span class="bm-loading-spinner lg"></span> Loding...</p>
                    </template>
                </div>
            </template>

        </div>
        <template v-if="list === null && loading === false">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="alert-inner--icon"><i class="ni ni-bell-55"></i></span>
                <span class="alert-inner--text">{{ mgsErrorList }}</span>
            </div>
        </template>
        <template v-else-if="loading === true && list === null">
            <div class="flex-center">
                <span class="bm-loading-spinner lg"></span>
            </div>
        </template>
        <template v-else>
            <div class="row list-header"> 
                <div class="col-lg-1 col-md-1 col-sm-12">
                    №
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12">
                    Name
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    Description
                </div>
            </div>
            <template v-for="(item,index) in list">
                 <div class="row list-row"> 
                    <div class="col-lg-1 col-md-1 col-sm-12">
                        {{ index+1 }}
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12">
                        {{ item.name }}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        {{ item.description }}
                    </div>
                </div>
            </template>
        </template>
        <site-add v-on:addModel="addItem" :add-model="addModel" ></site-add>    
   </div>       
    `,
    created: function () {
        this.updateList();
        this.$on('addModel');

    }
});

/**
 * Добавления нового Site
 */
Vue.component('site-add',{
    props:["addModel"],
    data:function(){
        return{
            msgError: null
        }
    },
    methods:{
        updateModal:function(){
            this.msgError = null;
            if (this.addModel.name === null){
                this.msgError = "field name must not be empty";
                return false;
            }
            this.$emit('addModel');
            $('#modal-add-site').modal('hide');
        }
    },
    template: `
    <div class="modal fade" id="modal-add-site" tabindex="-1" role="dialog" aria-labelledby="modal-default" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h6 class="modal-title" id="modal-title-default"><i class="fas fa-folder-plus"></i></i>&nbsp;Add Site</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">
                          <div class="form-group">
                            <input type="text" placeholder="Name" v-model="addModel.name" class="form-control form-control-alternative">
                          </div>
                        </div>
                         <div class="col-lg-12 col-sm-12">
                          <div class="form-group">
                            <textarea rows="5" cols="5" placeholder="Description" v-model="addModel.description" class="form-control form-control-alternative"></textarea>
                          </div>
                        </div>
                       <!-- <div class="col-lg-12 col-sm-12">
                            <div class="custom-control custom-checkbox mb-3">
                              <input class="custom-control-input" id="customCheck1" type="checkbox" v-model="addModel.apiAuthorization">
                              <label class="custom-control-label" for="customCheck1">
                                <span>Api authorization</span>
                              </label>
                            </div>
                        </div>-->
                        <div class="col-lg-12 col-sm-12">
                            <div class="form-group">
                                <template v-if="msgError !== null">
                                    <div class="alert alert-warning alert-dismissible" role="alert">
                                        <span class="alert-inner--icon"><i class="ni ni-bell-55"></i></span>
                                         <span class="alert-inner--text">{{ msgError }}</span>
                                      </div>
                                   
                                </template>                
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary" v-on:click="updateModal">Add</button>
                  <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
    `
});
