//Найстройка SITES
Vue.component('user-block', {
    props: {
        addEl: {
            default: false
        },
        isAdmin: {
            default: false
        }
    },
    data: function () {
        return {
            list: null,
            loading: true,
            mgsErrorList: "User list is emptyt out!",
            msgInfo: null,
            loadingAdd: false,
            addModel: {
                name: null,
                password: null,
                type: null,
                email: null
            }
        }
    },
    mixins: [httpClientMixin],
    methods: {
        updateList: function () {
            this.loading = true;
            this.list = null;
            var uri = "/web/ajax/users-get-list";
            this.httpClient(uri, "GET", null, (result) => {
                if ("data" in result && result.data.length > 0) {
                    this.list = result.data;
                }
                //  console.log(this.list);
                this.loading = false
            });


        },
        addItem: function () {
            var uri = "/web/ajax/user-add";
            this.msgInfo = null;
            this.loadingAdd = true;
            this.httpClient(uri, "POST", this.addModel, (result) => {
                this.loadingAdd = false;
                this.msgInfo = result.message;
                this.updateList();
                if (result.succes) {
                    this.addModel= {
                        name: null,
                        password: null,
                        type: null,
                        email: null
                    };


                }
                var self = this;
                setTimeout(() => self.msgInfo = null, 10000);
            });
        },
        isBool: function (s) {
            if (parseInt(s) === 1) return "YES";
            return "NO";
        }
    },
    template: `
   <div>
        <div class="row"> 
            <template v-if="JSON.parse(addEl)"> 
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <button type="button" class="btn btn-block btn-success mb-3" data-toggle="modal" data-target="#modal-add-site">
                        <i class="fas fa-users-cog"></i>&nbsp;Add user
                    </button>   
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12">
                    <template v-if="msgInfo !== null">
                       <div class="alert alert-info alert-dismissible min-allert" role="alert">
                        <span class="alert-inner--icon"><i class="ni ni-bell-55"></i></span>
                        <span class="alert-inner--text">{{ msgInfo }}</span>
                      </div>
                    </template>
                    <template v-if="msgInfo !== null && loadingAdd === true">
                        <p class="text-info mb-0"><span class="bm-loading-spinner lg"></span> Loding...</p>
                    </template>
                </div>
            </template>
        </div>
        <template v-if="list === null && loading === false">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <span class="alert-inner--icon"><i class="ni ni-bell-55"></i></span>
                <span class="alert-inner--text">{{ mgsErrorList }}</span>
            </div>
        </template>
        <template v-else-if="loading === true && list === null">
            <div class="flex-center">
                <span class="bm-loading-spinner lg"></span>
            </div>
        </template>
        <template v-else>
            <div class="row list-header"> 
                <div class="col-lg-1 col-md-1 col-sm-12">
                    №
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    Login
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12">
                    Email
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12">
                    Admin
                </div>
                 <div class="col-lg-1 col-md-1 col-sm-12">
                    Active
                </div>
            </div>
            <template v-for="(item,index) in list">
                 <div class="row list-row"> 
                    <div class="col-lg-1 col-md-1 col-sm-12">
                        {{ index+1 }}
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        {{ item.username }}
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12">
                        {{ item.email }}
                    </div>
                      <div class="col-lg-1 col-md-1 col-sm-12">
                        {{ isBool(item.isAdmin) }}
                    </div>
                   <div class="col-lg-1 col-md-1 col-sm-12">
                        {{ isBool(item.confirmed_by_email) }}
                    </div>
                </div>
            </template>
        </template>
        <user-add v-on:addModel="addItem" :add-model="addModel" :is-admin="isAdmin"></user-add>    
   </div>       
    `,
    created: function () {
        this.updateList();
        this.$on('addModel');

    }
});

/**
 * Добавления нового Site
 */
Vue.component('user-add', {
    props: ["addModel", "isAdmin"],
    data: function () {
        return {
            msgError: null
        }
    },
    methods: {
        updateModal: function () {
            this.msgError = null;
            if (this.addModel.name === null ||
                this.addModel.email === null ||
                this.addModel.password === null ||
                this.addModel.type === null
            ) {
                this.msgError = "Fill in all the fields";
                return false;
            }
            this.$emit('addModel');
            $('#modal-add-site').modal('hide');
        }
    },
    computed: {
        listType: function () {
            var list = [
                {id: 3, name: "Manager"}
            ];

            if (JSON.parse(this.isAdmin))
                list = [
                    {id: 0, name: "Admin"},
                    {id: 2, name: "User"},
                    {id: 3, name: "Manager"}
                ];
            return list;

        }
    },
    template: `
    <div class="modal fade" id="modal-add-site" tabindex="-1" role="dialog" aria-labelledby="modal-default" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h6 class="modal-title" id="modal-title-default"><i class="fas fa-folder-plus"></i></i>&nbsp;Add User</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">
                          <div class="form-group">
                            <input type="text" placeholder="Name" v-model="addModel.name" class="form-control form-control-alternative">
                          </div>
                        </div>
                         <div class="col-lg-12 col-sm-12">
                          <div class="form-group">
                            <input type="text" placeholder="Email" v-model="addModel.email" class="form-control form-control-alternative">
                          </div>
                        </div>
                         <div class="col-lg-12 col-sm-12">
                          <div class="form-group">
                            <input type="password" placeholder="Password" v-model="addModel.password" class="form-control form-control-alternative">
                          </div>
                        </div>
                        <div class="col-lg-12 col-sm-12">
                            <template v-if="listType !== null">
                                <div class="form-group">
                                    <select class="form-control form-control-alternative" v-model="addModel.type"> 
                                        <template v-for="(item,index) in listType">
                                            <option :value="item.id">{{ item.name }}</option> 
                                        </template>
                                    </select>
                                    </div>
                            </template>
                        </div>
                        <div class="col-lg-12 col-sm-12">
                            <div class="form-group">
                                <template v-if="msgError !== null">
                                    <div class="alert alert-warning alert-dismissible" role="alert">
                                        <span class="alert-inner--icon"><i class="ni ni-bell-55"></i></span>
                                         <span class="alert-inner--text">{{ msgError }}</span>
                                      </div>
                                   
                                </template>                
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary" v-on:click="updateModal">Add</button>
                  <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
    `
});
