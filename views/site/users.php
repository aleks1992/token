<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="user-block">
    <user-block add-el=<?= json_encode($addEl); ?> is-admin=<?= json_encode($isAdmin); ?>></user-block>
</div>
