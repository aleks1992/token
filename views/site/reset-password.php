<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container pt-lg-md">
    <div class="row justify-content-center">
        <div class="col-lg-5">
            <div class="card bg-secondary shadow border-0">
                <div class="card-header bg-white pb-5">
                    You can request your password by providing your user name and the password will be sent to the e-mail address you provided during registration.
                </div>
                <div class="card-body px-lg-5 py-lg-5">
                    <?php $form = ActiveForm::begin([
                        'id' => 'reset-password-form',
                        'layout' => 'horizontal',
                        'fieldConfig' => [],
                    ]); ?>

                    <?= $form->field($model, 'email', [
                            'template' => " 
                                        <div class=\"form-group mb-3\">
                                            <div class=\"input-group input-group-alternative\">
                                                <div class=\"input-group-prepend\">
                                                    <span class=\"input-group-text\"><i class=\"ni ni-email-83\"></i></span>
                                                </div>
                                                {input}
                                            </div>
                                        </div>
                                        <div class=\"text-danger\">{error}</div>"]
                    )->textInput(['autofocus' => true])->input('email', ['placeholder' => "Enter Your Email"])->label(false); ?>

                    <div class="form-group">
                        <?= Html::submitButton('Reset password', ['class' => 'btn btn-primary my-4 btn-block', 'name' => 'reset-password-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-6">
                    <a href="login" class="text-light"><small>Login</small></a>
                </div>
                <div class="col-6 text-right">
                    <a href="register" class="text-light"><small>Create new account</small></a>
                </div>
            </div>
        </div>
    </div>
</div>
