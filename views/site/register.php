<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container pt-lg-md">
    <div class="row justify-content-center">
        <div class="col-lg-5">
            <div class="card bg-secondary shadow border-0">
<!--                <div class="card-header bg-white pb-5">
                    <div class="text-muted text-center mb-3"><small>Sign in with</small></div>
                    <div class="btn-wrapper text-center">
                        <a href="#" class="btn btn-neutral btn-icon btn-circle btn-circle-social">
                            <span class="btn-inner--icon">
                              <img alt="image" src="../assets/argon/img/icons/common/github.svg">
                            </span>
                        </a>
                        <a href="#" class="btn btn-neutral btn-icon btn-circle btn-circle-social">
                            <span class="btn-inner--icon">
                              <img alt="image" src="../assets/argon/img/icons/common/google.svg">
                            </span>
                        </a>
                    </div>
                </div>-->
                <div class="card-body px-lg-5 py-lg-5">
                    <div class="text-center text-muted mb-4">
                        <small>Register</small>
                    </div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'register-form',
                        'layout' => 'horizontal',
                        'fieldConfig' => [],
                    ]); ?>

                    <?= $form->field($model, 'username', [
                            'template' => " 
                                        <div class=\"form-group mb-3\">
                                            <div class=\"input-group input-group-alternative\">
                                                <div class=\"input-group-prepend\">
                                                    <span class=\"input-group-text\"><i class=\"ni ni-hat-3\"></i></span>
                                                </div>
                                                {input}
                                            </div>
                                        </div>
                                        <div class=\"text-danger\">{error}</div>"]
                    )->textInput(['autofocus' => true])->input('text', ['placeholder' => "Name"])->label(false); ?>

                    <?= $form->field($model, 'email', [
                            'template' => " 
                                        <div class=\"form-group mb-3\">
                                            <div class=\"input-group input-group-alternative\">
                                                <div class=\"input-group-prepend\">
                                                    <span class=\"input-group-text\"><i class=\"ni ni-email-83\"></i></span>
                                                </div>
                                                {input}
                                            </div>
                                        </div>
                                        <div class=\"text-danger\">{error}</div>"]
                    )->textInput()->input('email', ['placeholder' => "Email"])->label(false); ?>

                    <?= $form->field($model, 'password', ['template' => " 
                                        <div class=\"form-group mb-3\">
                                            <div class=\"input-group input-group-alternative\">
                                                <div class=\"input-group-prepend\">
                                                    <span class=\"input-group-text\"><i class=\"ni ni-lock-circle-open\"></i></span>
                                                </div>
                                                {input}
                                            </div>
                                        </div>
                                        <div class=\"text-danger\">{error}</div>
                                        "])->passwordInput()->input('password', ['placeholder' => "Password"])->label(false); ?>

                    <div class="form-group">
                        <?= Html::submitButton('Register', ['class' => 'btn btn-primary my-4 btn-block', 'name' => 'register-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
