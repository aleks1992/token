<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container pt-lg-md">
    <div class="row justify-content-center">
        <div class="col-lg-5">
            <div class="card bg-secondary shadow border-0">
                <div class="card-header bg-white pb-5">
                    <?php if (isset($isReset)):?>

                        Your password has been successfully changed
                    <?php else:?>
                        Error: You do not have permission to access the requested module
                    <?endif;?>
                    <a class="btn btn-primary my-4 btn-block" href="/web/">Home</a>
                </div>
            </div>
        </div>
    </div>
</div>
