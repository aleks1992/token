<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset Password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container pt-lg-md">
    <div class="row justify-content-center">
        <div class="col-lg-5">
            <div class="card bg-secondary shadow border-0">

                <div class="card-body px-lg-5 py-lg-5">
                    <div class="text-center text-muted mb-4">
                        <small>Reset Password</small>
                    </div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'update-password-form',
                        'layout' => 'horizontal',
                        'fieldConfig' => [],
                    ]); ?>
                    <div style="display: none">
                        <?= $form->field($model, 'email', [
                                'template' => " 
                                        <div class=\"form-group mb-3\">
                                            <div class=\"input-group input-group-alternative\">
                                                <div class=\"input-group-prepend\">
                                                    <span class=\"input-group-text\"><i class=\"ni ni-email-83\"></i></span>
                                                </div>
                                                {input}
                                            </div>
                                        </div>
                                        <div class=\"text-danger\">{error}</div>"]
                        )->textInput(['autofocus' => true])->input('email', ['placeholder' => "Email"])->label(false); ?>
                        <?= $form->field($model, 'code', [
                                'template' => " 
                                        <div class=\"form-group mb-3\">
                                            <div class=\"input-group input-group-alternative\">
                                                <div class=\"input-group-prepend\">
                                                    <span class=\"input-group-text\"><i class=\"ni ni-lock-circle-open\"></i></span>
                                                </div>
                                                {input}
                                            </div>
                                        </div>
                                        <div class=\"text-danger\">{error}</div>"]
                        )->textInput(['autofocus' => true])->input('text', ['placeholder' => "Code"])->label(false); ?>


                    </div>
                    <div>
                        <?= $form->field($model, 'password', ['template' => " 
                                        <div class=\"form-group mb-3\">
                                            <div class=\"input-group input-group-alternative\">
                                                <div class=\"input-group-prepend\">
                                                    <span class=\"input-group-text\"><i class=\"ni ni-lock-circle-open\"></i></span>
                                                </div>
                                                {input}
                                            </div>
                                        </div>
                                        <div class=\"text-danger\">{error}</div>
                                        "])->passwordInput()->input('password', ['placeholder' => "Password"])->label(false); ?>

                        </div>

                    <div class="form-group">
                        <?= Html::submitButton('save', ['class' => 'btn btn-primary my-4 btn-block', 'name' => 'reset-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-6">
                    <a href="reset-password" class="text-light"><small>Forgot password?</small></a>
                </div>
                <div class="col-6 text-right">
                    <a href="register" class="text-light"><small>Create new account</small></a>
                </div>
            </div>
        </div>
    </div>
</div>
