<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Cards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="cards-block">
    <cards-block add-el=<?= json_encode($addEl); ?> is-admin=<?= json_encode($isAdmin); ?>></cards-block>
</div>
