<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Sites';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="sites-block">
    <sites-block add-el=<?= json_encode($addEl); ?> is-admin=<?= json_encode($isAdmin); ?>></sites-block>
</div>
