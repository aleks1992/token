<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container pt-lg-md">
    <div class="row justify-content-center">
        <div class="col-lg-5">
            <div class="card bg-secondary shadow border-0">
                <div class="card-body px-lg-5 py-lg-5">
                    An email will also be sent to your address to confirm that your registration have completed successfully.
                    <a href="/web" class="btn btn-primary my-4 btn-block"> Home </a>
                </div>
            </div>
        </div>
    </div>
</div>
