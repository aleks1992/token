<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Confirmed Email';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container pt-lg-md">
    <div class="row justify-content-center">
        <div class="col-lg-5">
            <div class="card bg-secondary shadow border-0">
                <div class="card-header bg-white pb-5">
                    <?php if (isset($isConfirmedEmail) && $isConfirmedEmail):?>
                        Email successfully verified
                    <?php else:?>
                        Email verification Error
                    <?endif;?>
                    <a class="btn btn-primary my-4 btn-block" href="/site">Home</a>
                </div>
            </div>
        </div>
    </div>
</div>
