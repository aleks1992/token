<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Setting';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="setting-block">
    <setting-block ga-secret="<?= $ga_secret; ?>" uri="<?= $uri; ?>"></setting-block>
</div>
