<?php


namespace app\models;
use Yii;
use yii\base\Model;

class RegisterForm  extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_confirmation;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }


    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function register()
    {

        if (!$this->validate()) {
            return null;
        }


        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        if ($user->save()){
            //Отправка Email
            $uri = Yii::$app->urlManager->getHostInfo() . Yii::$app->urlManager->createUrl(['site/confirmed-email', "code" => $user->getAuthKey(), "email" => $this->email]);

            Yii::$app->mailer->compose()
                ->setTo($this->email)
                ->setFrom([Yii::$app->params['supportEmail'] => 'Account Confirmations'])
                ->setSubject("Account Confirmations")
                ->setHtmlBody("Hello <br> To verify your account, go to <a href='" . $uri . "'>link</a>")
                ->send();
            return $user;
        }

        return null;
    }

}