<?php


namespace app\models;

use Yii;
use yii\base\Model;

class UpdatePasswordForm extends Model
{
    public $email;
    public $code;
    public $password;

    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['password', 'required'],
            ['code', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }


    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function update()
    {
        if ($this->validate()) {
            $user = (new \yii\db\Query())
                ->select(['*'])
                ->from('users')
                ->where(["email" => $this->email, "password_reset_token" => $this->code])
                ->one();

            if (is_array($user) && array_key_exists('id', $user)) {
                Yii::$app->db->createCommand()->update('users',
                    [
                        'password_hash' => Yii::$app->security->generatePasswordHash($this->password)
                    ],
                    "id = :id", ['id' => (int)$user['id']]
                )->execute();
                return  true;
            }
        }
        return false;

    }

}