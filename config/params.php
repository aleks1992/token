<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'supportEmail' => 'kharin.92@yandex.ru',
    'googleAccounts' => 'https://support.google.com/accounts/answer/1066447?hl=ru',
    'isTwoFactory' => true
];
