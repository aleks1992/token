<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%card}}`.
 */
class m200118_072608_create_card_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cards', [
            'id' => $this->primaryKey(),
            'userId'=>$this->integer()->notNull(),
            'siteId'=>$this->integer()->notNull(),
            'pinCode'=>$this->text()->notNull(),
            'description'=>$this->text()
        ]);

        $this->addForeignKey(
            'userIdCards',
            'cards',
            'userId',
            'users',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'siteIdCards',
            'cards',
            'siteId',
            'sites',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cards');
    }
}
