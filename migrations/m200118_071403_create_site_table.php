<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%site}}`.
 */
class m200118_071403_create_site_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sites', [
            'id' => $this->primaryKey(),
            'userId'=>$this->integer()->notNull(),
            'name'=>$this->text()->notNull(),
            'description'=>$this->text(),
            'geoLocationLat'=>$this->double(),
            'geoLocationLon'=>$this->double(),
            'apiToken'=>$this->text(),
            'isApiToken'=>$this->boolean()->defaultValue(false)
        ]);

        $this->addForeignKey(
            'userId',
            'sites',
            'userId',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sites');
    }
}
