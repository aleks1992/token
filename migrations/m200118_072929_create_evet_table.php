<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%evet}}`.
 */
class m200118_072929_create_evet_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('events', [
            'id' => $this->primaryKey(),
            'cardId'=>$this->integer()->notNull(),
            'timeIn'=>$this->dateTime(),
            'timeOut'=>$this->dateTime()
        ]);

        $this->addForeignKey(
            'cardIdEvents',
            'evets',
            'cardId',
            'cards',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('events');
    }
}
