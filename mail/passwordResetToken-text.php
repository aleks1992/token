<?php
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/update-password', 'code' => $user->password_reset_token,'email'=>$user->email]);
?>

Hello <?= $user->username ?>,
Follow the link below to reset your password:

<?= $resetLink ?>
